var timeFormatText = (function (){
    function num(val){
        val = Math.floor(val);
        return val < 10 ? '0' + val : val;
    }

    return function (sec){
        var  hours = sec / 3600  % 24
          , minutes = sec / 60 % 60
          , seconds = sec % 60
        ;

        return num(hours) + ":" + num(minutes) + ":" + num(seconds);
    };
})();

function autoLoadSpriteImg(sprite, imgUrl){
    var timeStamp = Date.now();
    loader = new P.Loader(G);
    loader.image(timeStamp, imgUrl );
    loader.onLoadComplete.addOnce(function(){
        sprite.loadTexture(timeStamp);
    });
    loader.start();
}

function addLabelToElementCenter(layer,label,style){
	var  _label = gamePhaser.add.text(layer.width / 2, layer.height / 2, label, style);
    _label.anchor.x = 0.5;
    _label.anchor.y = 0.5;
	layer.addChild(_label);
	return _label;
}

function addLabelToElementCenterTop(layer,label,style){
	var  _label = G.add.text(layer.width / 2, layer.height / 2, label, style);
    _label.anchor.x = 0.5;
    _label.anchor.y = 0.5;
	layer.addChild(_label);
    return _label;
}

function addLabelToElementLeftCenter(layer,label,style,padding){
	if (padding === undefined || padding === null) padding = 30;
	var  _label = G.add.text(padding, layer.height / 2, label, style);
    _label.anchor.x = 0;
    _label.anchor.y = 0.5;
	layer.addChild(_label);
	return _label;
}

function positionLabelCenter(x, y, label, style){
    var _label = G.add.text(x, y, label, style);
    _label.anchor.x = 0.5;
    return _label;
}

function positionLoaderFromPercent(s, e, p){
    return ((e - s)/100)*p + s;
}

function numberToNumbersN(n, c){
    var x = ( (n/c) - parseInt(n/c) > 0.499 ? (parseInt(n/c)+1): parseInt(n/c));
    var a = [];
    var t = 0;
    for (var i = 0; i < (c-1); i++){
        t += x;
        a.push(x);
    }
    a.push(n - t);
    return a;
}

function updateFontSize(label, text, width, current_size, min_size){
    if (min_size == null) min_size = 10;
    label.setText(text);
    var i = current_size;
    for (; i > min_size; i--){
        label.fontSize = i;
        if (label.width <= width) break;
    }
    return i;
}

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}
