Game.MainMenu = function(gamePhaser) {

};

Game.MainMenu.prototype.preload = function()
{
   this.load.image('button00','../images/ui/red_button00.png');
}

Game.MainMenu.prototype.create = function()
{
  gamePhaser.scale.scaleMode = P.scaleModes.LINEAR;
  this.stage.backgroundColor = '#182d3b';
  var button = this.add.button(this.world.centerX, this.world.centerY, 'button00', actionOnClick, this);
  button.anchor.setTo(0.5);
  var  label = this.add.text(button.width  / 8, button.height / 2, "Play");
  label.anchor.x = 1;
  label.anchor.y = 1;
  button.addChild(label);
}
function actionOnClick() {
  this.state.start('Level1');
}
