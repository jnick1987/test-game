Game.Level1 = function(gamePhaser){}

var player;

var grounds;
var ground_width =0;

var block;

var EasyLand;
var EasyMaps = [];
var MidleLand;
var MidleMaps = [];
var HardLand;
var HardMaps = [];
var ShownMaps = [];
var controls={};
var playerSpeed = 250;
var jumpTimer = 0;


Game.Level1.prototype.preload = function () {
  this.load.spritesheet('player', '../images/player.png',70,100);
  this.load.image('platform', '../images/platform1.png');
  this.load.image('2x1', '../images/2x1.png');
  this.load.image('1x1', '../images/1x1.png');
  this.load.image('3x1', '../images/3x1.png');
  this.load.image('1x2', '../images/1x2.png');
  this.load.image('1x3', '../images/1x3.png');
  this.load.image('2x2', '../images/2x2box.png');
}


Game.Level1.prototype.create = function () {

  grounds = this.add.group();
  grounds.enableBody = true;
  grounds.physicsBodyType = P.Physics.ARCADE;
  this.physics.arcade.gravity.y = 1400;


  this.createPlayer();
  // Земля

  while (ground_width <= 800) {
    this.createGround(ground_width);
    ground_width += 80*10;
  }


  this.createHardLands();
  this.showHardLands();
}


Game.Level1.prototype.update = function () {
  this.physics.arcade.collide(player,grounds);
  this.physics.arcade.collide(player,EasyLand);
  this.physics.arcade.collide(player,MidleLand);
  this.physics.arcade.collide(player,HardLand);
  if (controls.up.isDown && (player.body.onFloor() || player.body.touching.down) && this.time.now > jumpTimer && player.animations.name != 'duck') {
    player.body.velocity.y  = -700 ;
    player.animations.play('jump');
    jumpTimer = this.time.now + 750;
  }
  if (player.body.velocity.y == 0) {
      player.animations.play('idle');
  }
  if(controls.down.isDown && (player.body.onFloor()|| player.body.touching.down)){
    player.animations.play('duck');
  }
  // проверка на нахождения персонажа поверх какого-то блока
  if(player.body.touching.down){
    player.body.velocity.x = playerSpeed;
  }
  else {
    player.body.velocity.x = 0;
  }
  // проверка на косание боком, в случа этого игрок проигрывает
  if (player.body.touching.left || player.body.touching.right) {
    this.stage.backgroundColor = '#800000';
  }
  //Когда блок появляется на экране, включается проверка на его выход с экрана
  if(block.body.position.x < 800 && !block.checkWorldBounds){
    block.checkWorldBounds = true;
  }
}


Game.Level1.prototype.render = function () {

}


//Функция создания игрока
Game.Level1.prototype.createPlayer = function() {
  player = this.add.sprite(this.world.centerX, this.world.height - 200, 'player',0);
  player.anchor.setTo(0.5);
  this.physics.arcade.enable(player);
  this.camera.follow(player);
  player.body.collideWorldBounds = true;
  player.animations.add('idle',[7,11,12],6,true);
  player.animations.add('jump',[6],0,false);
  player.animations.add('duck',[5],5,false);
  controls = {
    up: this.input.keyboard.addKey(P.Keyboard.W),
    down: this.input.keyboard.addKey(P.Keyboard.S),
  };
}

// Функция отрисовки земли
Game.Level1.prototype.createGround  = function (ground_width) {
        g = grounds.create(ground_width, gamePhaser.world.height, 'platform');
        gamePhaser.physics.arcade.enable(g);
        g.anchor.setTo(0,0.5);
        g.body.velocity.x  = -playerSpeed;
        g.body.allowGravity = false;
        g.body.immovable = true;
        g.checkWorldBounds = true;
        g.events.onOutOfBounds.add(function(g) {
          g.kill();
          Game.Level1.prototype.createGround(gamePhaser.world.width - 5);
        });
}

//Функция отрисовкм блока
Game.Level1.prototype.createBlock  = function (x = 0 , y = 0, size) {
  block = gamePhaser.add.sprite (gamePhaser.world.width + x, gamePhaser.world.height - 40 - y, size);
  gamePhaser.physics.arcade.enable(block);
  block.anchor.setTo(0,1);
  block.body.allowGravity = false;
  block.body.immovable = true;
  block.body.velocity.x = -playerSpeed;
  block.events.onOutOfBounds.add(function(block) {
  block.kill();
  });
  return block;
}


// Функция создания лёгких препядствий
Game.Level1.prototype.createEasyLands = function(){
  EasyLand = this.add.group();
  EasyLand.enableBody = true;
  EasyLand.physicsBodyType = P.Physics.ARCADE;

  EasyMaps.push(function(){
    EasyLand.add(Game.Level1.prototype.createBlock(0,0,'1x1'));
    EasyLand.add(Game.Level1.prototype.createBlock(210,0,'2x1'));
     EasyLand.add(Game.Level1.prototype.createBlock(420,0,'3x1'));
  });
  EasyMaps.push(function(){
    EasyLand.add(Game.Level1.prototype.createBlock(0,0,'2x1'));
    EasyLand.add(Game.Level1.prototype.createBlock(210,0,'2x1'));
     EasyLand.add(Game.Level1.prototype.createBlock(420,0,'2x1'));
  });
}

// Функция создания средних препядствий
Game.Level1.prototype.createMidleLands = function(){
  MidleLand = this.add.group();
  MidleLand.enableBody = true;
  MidleLand.physicsBodyType = P.Physics.ARCADE;

  MidleMaps.push(function(){
    MidleLand.add(Game.Level1.prototype.createBlock(0,0,'1x3'));
    MidleLand.add(Game.Level1.prototype.createBlock(350,70,'1x3'));
     MidleLand.add(Game.Level1.prototype.createBlock(700,0,'3x1'));
     MidleLand.add(Game.Level1.prototype.createBlock(980,70,'1x3'));
  });
  MidleMaps.push(function(){
  MidleLand.add(Game.Level1.prototype.createBlock(0,70,'1x3'));
  MidleLand.add(Game.Level1.prototype.createBlock(420,0,'2x1'));
  MidleLand.add(Game.Level1.prototype.createBlock(700,70,'1x3'));
  });
}


// Функция создания тяжелых препядствий
Game.Level1.prototype.createHardLands = function(){
 HardLand = this.add.group();
 HardLand.enableBody = true;
  HardLand.physicsBodyType = P.Physics.ARCADE;

  HardMaps.push(function(){
   HardLand.add(Game.Level1.prototype.createBlock(0,0,'2x1'));
    HardLand.add(Game.Level1.prototype.createBlock(280,70,'1x3'));
     HardLand.add(Game.Level1.prototype.createBlock(420,210,'1x2'));
     HardLand.add(Game.Level1.prototype.createBlock(630,350,'1x3'));
     HardLand.add(Game.Level1.prototype.createBlock(700,0,'2x1'));
     HardLand.add(Game.Level1.prototype.createBlock(840,140,'1x3'));
  });
}

//Функция отображения лёгких препядтсвий
Game.Level1.prototype.showEasyLands = function(){ 
  if (ShownMaps.length < EasyMaps.length) {
    var rand = Math.floor(Math.random() * (EasyMaps.length));
    if (ShownMaps.indexOf(rand) == -1) {
      ShownMaps.push(rand);
      console.log(ShownMaps);
      EasyMaps[rand]();
      this.time.events.add(P.Timer.SECOND * 4, this.showEasyLands,this);
    }
    else{
      this.showEasyLands();
    }
  }
}

//Функция отображения средних препядтсвий
Game.Level1.prototype.showMidleLands = function(){ 
  if (ShownMaps.length < MidleMaps.length) {
    var rand = Math.floor(Math.random() * (MidleMaps.length));
    if (ShownMaps.indexOf(rand) == -1) {
      ShownMaps.push(rand);
      console.log(ShownMaps);
      MidleMaps[rand]();
      this.time.events.add(P.Timer.SECOND * 4, this.showMidleLands,this);
    }
    else{
      this.showMidleLands();
    }
  }
}

// Функция отображения тяжелых препядствий
Game.Level1.prototype.showHardLands = function(){ 
  if (ShownMaps.length < HardMaps.length) {
    var rand = Math.floor(Math.random() * (HardMaps.length));
    if (ShownMaps.indexOf(rand) == -1) {
      ShownMaps.push(rand);
      console.log(ShownMaps);
      HardMaps[rand]();
      this.time.events.add(P.Timer.SECOND * 4, this.showHardLands,this);
    }
    else{
      this.showHardLands();
    }
  }
}