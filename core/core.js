var g_constants = {
    version: "0.0.1",
    width: 800,
    height: 600
};
var P = Phaser;
var gamePhaser = null;
var game = null;

function document_loaded() {

    startGame();

    native_exec({type:"loaded"});
}

function startGame(){
    game = new Game();
    gamePhaser = new P.Game(g_constants.width, g_constants.height,
            P.CANVAS, "game", {preload: game.preload.bind(game), create: game.create.bind(game),
            update: game.update.bind(game), render: game.render.bind(game)});

    gamePhaser.state.add('MainMenu', Game.MainMenu);
    gamePhaser.state.add('Level1', Game.Level1);

    gamePhaser.state.start('MainMenu');
  }

function native_exec(data) {
    if (typeof data == 'object') {
        data = JSON.stringify(data);
    }
    else if (typeof data != 'string') data = data.toString();

    console.log("exec: "+data);
    if (typeof native != "undefined") native.exec(data);
    else console.error("native is null! data: "+data);
}
